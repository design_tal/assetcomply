<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyUser extends Model
{
    protected $table='verify_users';

    protected $fillable = [
        'username', 'token'
    ];
}
