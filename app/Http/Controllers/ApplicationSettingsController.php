<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 

use App\Locations;
use App\Department;
use App\Unit;
use App\VendorCustomer;
use App\User;
use App\Userdetails;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;

class ApplicationSettingsController extends Controller
{
    
    /*Asset Location settings for a company*/

       public function setLocationForCompany(Request $request)
            {
                
                // $url = $request->fullUrl();
                
                // if (strpos($url, '=') || strpos($url, '&')) {

                //    Log::warning($request->ip().' is trying to pass values through url');
                
                // return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
                //   die();
                
                // }
                
                 $validator = Validator::make($request->all(), [
                            'loc_id' =>'required',
                            'location_name' => 'required',
                            'company_id' => 'required', 
                            'username' => 'required',                 
                  ]);
 
                    if($validator->fails()) {
                
                          //pass validator errors as errors object for ajax response
                        $message=$validator->errors()->first();

                              return response()->json(['status'=>"901",'message' => $message]);
                       }

                       else{
                        try{  

                          $address = new Locations();
                          $address->loc_id=$request->input('loc_id');
                          $address->location_name=$request->input('location_name');
                          $address->company_id= strtolower($request->input('company_id')); 
                          $address->added_by=$request->input('username');
                          $address->delete_status= 0;
                          $address->save();

                        }catch (\Exception $e) {

                              Log::error($e->getMessage());
                              return response()->json(['status' => "902",'message' => "Exception while saving Location"],422);
                          }

                          Log::info('location saved');

                          return response()->json(['status'=>"900",'message' => "Address saved"]);

                       }


          }

//  Location List
  public function listLocationsByCompany($company_id)
  {
      $locations_data = Locations::where('company_id', strtolower($company_id))
                                    ->where('delete_status',0)
                                    ->orderBy('created_at', 'desc')
                                    ->paginate(10);
      return response()->json(['status'=>"900",'data' => $locations_data]);
  }

//  Location Delete

  public function deleteSingleLocation($location_id,$company_id)
  {
      $validator = Validator::make($request->all(), [
                            'location_id' =>'required',
                            'company_id' => 'required',              
                  ]);
 
            if($validator->fails()) {
                
                          //pass validator errors as errors object for ajax response
                  $message=$validator->errors()->first();

                  return response()->json(['status'=>"901",'message' => $message]);
            }

     $location_soft_delete = Locations::where('loc_id', $location_id)
                                        ->where('company_id', strtolower($company_id))
                                        ->update(['delete_status'=>1]);

      return response()->json(['status'=>"900",'message' => "Location deleted",'location' => $location_id]);
  }


// Location Update 
  public function updateSingleLocation(Request $request)
  { 
       $validator = Validator::make($request->all(), [
                            'location_id' =>'required',
                            'location_name' => 'required',              
                  ]);
 
            if($validator->fails()) {
                
                          //pass validator errors as errors object for ajax response
                  $message=$validator->errors()->first();

                  return response()->json(['status'=>"901",'message' => $message]);
            }
            
      $location_id = $request->input('location_id');
      $location_name=$request->input('location_name');

      $location_soft_delete = Locations::where('loc_id', $location_id)
                                        ->update(['location_name'=>$location_name]);

      return response()->json(['status'=>"900",'message' => "Location updated",'location' => $location_name]);
  }

	/*asset property creation*/
	public function assetPropertySet(request $request)
	{	


		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		 
		}
			
		 $validator = Validator::make($request->all(), [
            		'catagoryname' =>'required',
            		'catagorycode' =>'required',
                'username' => 'required',               
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status' => "910",'message' => $message]);
       		 }

       		 else{

       		 	$assetproperty= new AssetProperty();
       		 	$assetproperty->parentcatagoryname=$request->input('parentcatagoryname');
       		 	$assetproperty->catagoryname=$request->input('catagoryname');
       		 	$assetproperty->catagorycode=$request->input('catagorycode');
       		 	$assetproperty->defaulttransferduration=$request->input('defaulttransferduration');
       		 	$assetproperty->endoflife=$request->input('endoflife');
       		 	$assetproperty->added_by=$request->input('username');
       		 	$assetproperty->customized_by=$request->input('username');
       		 	$assetproperty->save();

            Log::info('Setting asset property');

       		 	return response()->json(['status' => "900",'message' => "Asset property applied"]);



       		 }




	}

	

	/*stutas*/

	public function assetStatusSet(Request $request)
	{
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {
      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

	
		$validator = Validator::make($request->all(), [
            		'status' =>'required',
                'username' => 'required',               
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		 else{

       		 	$status= new Status();
       		 	$status->statustype=$request->input('statustype');
       		 	$status->status=$request->input('status');
       		 	$status->added_by=$request->input('username');
       		 	$status->customized_by=$request->input('username');
       		 	$status->save();

            Log::info('Status saved');

       		 	return response()->json(['status'=>"900",'message' => "status saved"]);

       		 }


	}

	/*department*/
	public function createDepartment(Request $request)
	{
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {
      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}
	
		$validator = Validator::make($request->all(), [
            		'department' =>'required',
            		'departmentcode' =>'required',
                'username' => 'required', 
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		 else{

       		 	$department= new Department();
       		 	$department->department=$request->input('department');
       		 	$department->departmentcode=$request->input('departmentcode');
       		 	$department->contactperson=$request->input('contactperson');
       		 	$department->description=$request->input('description');
       		 	$department->added_by=$request->input('username');
       		 	$department->customized_by=$request->input('username');
       		 	$department->save();

            Log::info('Department saved');

       		 	return response()->json(['status'=>"900",'message' => "department saved"]);

       		 }


	}

	/*meter type*/

	public function createMeterType(Request $request)
	{	

		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {
      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		
		$validator = Validator::make($request->all(), [
            		'typeofmeter' =>'required',
                'username' => 'required', 
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910", 'message' => $message]);
       		 }

       		 else{

       		 	$metertype= new MeterType();
       		 	$metertype->typeofmeter=$request->input('typeofmeter');
       		 	$metertype->added_by=$request->input('username');
       		 	$metertype->customized_by=$request->input('username');
       		 	$metertype->save();

            Log::info('metertype created');

       		 	return response()->json(['status'=>"900",'message' => "metertype created"]);


       		 }




	}

	/*condition*/

	public function createCondition(Request $request)
	{
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}


		$validator = Validator::make($request->all(), [
            		'conditiondescription' =>'required',
                'username' => 'required', 
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		 else{

       		 	$condition = new Condition();
       		 	$condition->conditiondescription=$request->input('conditiondescription');	
       		 	$condition->added_by=$request->input('username');
       		 	$condition->customized_by=$request->input('username');
       		 	$condition->save();

            Log::info('condition set');

       		 	return response()->json(['status'=>"900",'message' => "condition set"]);


       		 }


	}

	/*reason*/

	public function createReason(Request $request)
	{	
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {
      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		$validator = Validator::make($request->all(), [
            		'reason' =>'required',
                'username' => 'required', 
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		else{

       			$reason= new Reason();
       			$reason->reason=$request->input('reason');
       			$reason->added_by=$request->input('username');
       			$reason->customized_by=$request->input('username');
       			$reason->save();

            Log::info('Reason set');

       			return response()->json(['status'=>"900",'message' => "reason set"]);
       		}


	}

	/*vendor /customer */

	public function createVendorCustomer(Request $request)
	{	
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		$validator = Validator::make($request->all(), [
            		'vendorname' =>'required',
                'username' => 'required', 
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		else{

       			$vendor = new VendorCustomer();
       			$vendor->vendorname=$request->input('vendorname');
       			$vendor->contactperson=$request->input('contactperson');
       			$vendor->contactnumber=$request->input('contactnumber');
       			$vendor->address1=$request->input('address1');
       			$vendor->address2=$request->input('address2');
       			$vendor->registrationcode=$request->input('registrationcode');
       			$vendor->email=$request->input('email');
       			$vendor->city=$request->input('city');
       			$vendor->state=$request->input('state');
       			$vendor->postalcode=$request->input('postalcode');
       			$vendor->pan=$request->input('pan');
       			$vendor->gst=$request->input('gst');
       			$vendor->cst=$request->input('cst');
       			$vendor->customer=$request->input('VendorCustomer');
       			$vendor->added_by=$request->input('username');
       			$vendor->customized_by=$request->input('username');
       			$vendor->save();


            Log::info('Vendor details saved');

       			return response()->json(['status'=>"900",'message' => "vendor details saved"]);

       		}


	}

	/*tax*/

	public function createTax(Request $request)
	{	
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		
		$validator = Validator::make($request->all(), [
            		'taxname' =>'required',
            		'taxrate' =>'required',
            		'taxgroupname' =>'required',
                'username' => 'required', 
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		else{

       			$tax = new Tax();
       			$tax->taxname=$request->input('taxname');
       			$tax->taxrate=$request->input('taxrate');
       			$tax->taxgroupname=$request->input('taxgroupname');
       			$tax->added_by=$request->input('username');
       			$tax->customized_by=$request->input('username');
       			
       			$tax->save();

            Log::info('Tax details saved');

       			return response()->json(['status'=>"900",'message' => "Tax details saved"]);

       		}


	}


	/*activity type*/

	public function createActivityType(Request $request)
	{
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		
		$validator = Validator::make($request->all(), [
            		'activitytype' =>'required',
            		'amounttype' =>'required',
                'username' => 'required', 
            		
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		else{

       			$activity = new ActivityType();
       			$activity->ActivityType=$request->input('activitytype');
       			$activity->amounttype=$request->input('amounttype');

       			if($request->hasFile('file')){

       			// photo
                    $activityfile = $request->file('file');
                    $directory_file = 'activitytype'.'/'."user".'/';
                    $filename_activityfile = time() . '.' . $activityfile->getClientOriginalExtension();

           
                    $path_file = $activityfile->move($directory_file, $filename_activityfile);
                    $activity->file=$filename_activityfile;

       			}

       			else{
       				$activity->file="default.jpg";
       			}

       			
       			$activity->added_by=$request->input('username');
       			$activity->customized_by=$request->input('username');
       			
       			$activity->save();

            Log::info('Activity type saved');

       			return response()->json(['status'=>"900",'message' => "activity type saved"]);

       		}


	}


	/*movement status*/
    public function createMovementStatus(Request $request)
	{	
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		$validator = Validator::make($request->all(), [
            		'movementtype' =>'required',
            		'movementname' =>'required',
                'username' => 'required', 
            		
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		 else{

       		 	$movstatus= new MovementStatus();
       		 	$movstatus->movementtype=$request->input('movementtype');
       		 	$movstatus->movementname=$request->input('movementname');
       		 	$movstatus->added_by=$request->input('username');
       		 	$movstatus->customized_by=$request->input('username');
       		 	$movstatus->save();

            Log::info('movement status saved');

       		 	return response()->json(['status'=>"900",'message' => "movement status saved"]);

       		 }
		

	}

	/*unit settings*/
	public function createUnit(Request $request)
	{

		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		
		$validator = Validator::make($request->all(), [
            		'unit' =>'required',
                'username' => 'required', 
            		
            		
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }
       		 else{

       		 	$unit= new Unit();
       		 	$unit->unit=$request->input('unit');
       		 	$unit->description=$request->input('description');
       		 	$unit->added_by=$request->input('username');
       		 	$unit->customized_by=$request->input('username');
       		 	$unit->save();


            Log::info('unit saved');

       		 	return response()->json(['status'=>"900",'message' => "unit saved"]);

       		 }


	}

	/*brand*/
	public function createBrand(Request $request)
	{
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		$validator = Validator::make($request->all(), [
            		'brandname' =>'required',
                'username' => 'required',             		
            		
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }
       		 else{

       		 	$brand=new Brand();
       		 	$brand->brandname=$request->input('brandname');
       		 	$brand->added_by=$request->input('username');
       		 	$brand->customized_by=$request->input('username');
       		 	$brand->save();

            Log::info('brand created');

       		 	return response()->json(['status'=>"900",'message' => "brand created"]);


       		 }


	}

	/*model setting*/
	public function createModel(Request $request)
	{	

		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
 		
		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}


		$validator = Validator::make($request->all(), [
            		'modelname' =>'required',
                'username' => 'required',             		
            		
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }
       		 else{

       		 	$model=new Model();
       		 	$model->modelname=$request->input('modelname');
       		 	$model->added_by=$request->input('username');
       		 	$model->customized_by=$request->input('username');
       		 	$model->save();

            Log::info('model created');

       		 	return response()->json(['status'=>"900",'message' => "model created"]);

       		 }
		

	}

	/*purchase order setting*/

	public function createPurchaseOrder(Request $request)
	{
		
		$url = $request->fullUrl();
		
		if (strpos($url, '=') || strpos($url, '&')) {
 		
    Log::warning($request->ip().' is trying to pass values through url');

		return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
  		die();
		
		}

		
		$validator = Validator::make($request->all(), [
            		'ponumber' =>'required',
            		'header' =>'required',
            		'termsandconditions' =>'required',
            		'paymentterms' =>'required',
                'username' => 'required', 
            		
            		
			]);

            
            if($validator->fails()) {
    
    		//pass validator errors as errors object for ajax response
			$message=$validator->errors()->first();

          	return response()->json(['status'=>"910",'message' => $message]);
       		 }

       		 else{

       		 	$PurchaseOrder=new PurchaseOrder();
       		 	$PurchaseOrder->ponumber=$request->input('ponumber');
       		 	$PurchaseOrder->header=$request->input('header');
       		 	$PurchaseOrder->termsandconditions=$request->input('termsandconditions');
       		 	$PurchaseOrder->paymentterms=$request->input('paymentterms');

       		 	if($request->hasFile('file')){

       			// photo
                    $activityfile = $request->file('file');
                    $directory_file = 'PurchaseOrder/signature'.'/'."user".'/';
                    $filename_activityfile = time() . '.' . $activityfile->getClientOriginalExtension();

           
                    $path_file = $activityfile->move($directory_file, $filename_activityfile);
                    $PurchaseOrder->signaturefile=$filename_activityfile;

       			}

       			else{
       				$PurchaseOrder->signaturefile="default.jpg";
       			}
       		 	
       		 	$PurchaseOrder->added_by=$request->input('username');
       		 	$PurchaseOrder->customized_by=$request->input('username');
       		 	$PurchaseOrder->save();

            Log::info('purchase order added');

       		 	return response()->json(['status'=>"900",'message' => "purchase order added"]);

       		 }	


	}


  /*read settings*/

  public function assetPropertyView(request $request)
  {

        $validator = Validator::make($request->all(), [

                    'username' => 'required',               
          ]);
            
          if($validator->fails()) {
    
          //pass validator errors as errors object for ajax response
            $message=$validator->errors()->first();
            return response()->json(['status' => "910",'message' => $message]);
           }

           $result=AssetProperty::where('added_by',$request->input('username'))->sortBy('created_at','desc');

           return $result;
           die();




  }





}
