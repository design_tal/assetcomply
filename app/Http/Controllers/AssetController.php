<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\AssetInfo;
use Illuminate\Support\Facades\Storage;
use App\Imports\AssetImport;
use Maatwebsite\Excel\Facades\Excel;

class AssetController extends Controller
{
    public function addSingleAsset(Request $request)
    {
    	$asset_id = $request->input('asset_id');
      $asset_name = $request->input('asset_name');
  		$category  =$request->input('category');
  		$sub_category =$request->input('sub_category');
  		$brand 	  =$request->input('brand');
  		$model   =$request->input('model');
  		$serial_number	   =$request->input('serial_number');
  		$vendor_id	   =$request->input('vendor_id');
  		$description   =$request->input('description');
  		$warranty_date 	   =$request->input('warranty_date');
  		$condition   =$request->input('condition');
  		$po_number   =$request->input('po_number');

  		$invoice_number   =$request->input('invoice_number');
  		$invoice_date   =$request->input('invoice_date');
  		$purchase_price   =$request->input('purchase_price');
  		$purchase_date   =$request->input('purchase_date');
  		$capitalization_price  =$request->input('capitalization_price');
  		$end_of_life   =$request->input('end_of_life');
  		$depreciation_rate   =$request->input('depreciation_rate');
  		$capitalization_date   =$request->input('capitalization_date');
  		$scrap_value   =$request->input('scrap_value');
  		$department   =$request->input('department');
  		$date   =$request->input('date');

  		$state   =$request->input('state');
  		$city   =$request->input('city');
  		$location   =$request->input('location');
  		$units   =$request->input('units');
  		$emp_name   =$request->input('emp_name');
  		$emp_id   =$request->input('emp_id');
      $company   = strtolower($request->input('company'));

		 // return $asset_id;
		$validator = Validator::make($request->all(), [
            		    'asset_id'  =>'required',
                    'asset_name'  =>'required',
                    'category'  => 'required',
                    'sub_category'  =>'required',
                    'brand'  =>'required',
                    'serial_number'  => 'required|nullable',
                    'model'  =>'required',
                    'description'  =>'required',
                    'warranty_date'  => 'required|nullable|date',
                    'condition'  =>'required',
                    'vendor_id'  =>'required',
                    'po_number'   => 'nullable',
                    'invoice_number'  =>'nullable',
                    'invoice_date'  =>'date|nullable',
                    'purchase_price'  => 'numeric|nullable',
                    'purchase_date'  =>'date|nullable',
                    'capitalization_price'  =>'integer|nullable',
                    'end_of_life'  => 'date|nullable',
                    'depreciation_rate'  =>'integer|nullable',
                    'capitalization_date'  =>'date|nullable',
                    'scrap_value'  => 'integer|nullable',
                    'department'  =>'nullable',
                    'state'  =>'required',
                    'city'  => 'required',
                    'location'  =>'required',
                    'units'  =>'required|integer',
                    'emp_name'  => 'required',
                    'emp_id'  =>'required',
                    'date'  => 'required|date'                    
                 
			]);

           
            if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else {

       			try
                {
            // Asset ID checking 
       				$asset_id_check = AssetInfo::where('asset_id',$asset_id)->first();
       				  if(empty($asset_id_check)){

                   $asset_data = new AssetInfo();
                   $asset_data->asset_id    = $asset_id;
                   $asset_data->asset_name    = $asset_name;
                   $asset_data->category    = $category;
                   $asset_data->sub_category    = $sub_category;
                   $asset_data->brand    = $brand;
                   $asset_data->serial_number    = $serial_number;
                   $asset_data->model    = $model;
                   $asset_data->description    = $description;
                   $asset_data->warranty_date    = $warranty_date;
                   $asset_data->condition    = $condition;
                   $asset_data->vendor_id    = $vendor_id;
                   $asset_data->po_number    = $po_number;
                   $asset_data->invoice_number    = $invoice_number;
                   $asset_data->invoice_date    = $invoice_date;
                   $asset_data->purchase_price    = $purchase_price;
                   $asset_data->purchase_date    = $purchase_date;
                   $asset_data->capitalization_price    = $capitalization_price;
                   $asset_data->end_of_life    = $end_of_life;
                   $asset_data->depreciation_rate    = $depreciation_rate;
                   $asset_data->capitalization_date    = $capitalization_date;
                   $asset_data->scrap_value    = $scrap_value;
                   $asset_data->department    = $department;
                   $asset_data->state    = $state;
                   $asset_data->city    = $city;
                   $asset_data->location    = $location;
                   $asset_data->units    = $units;
                   $asset_data->emp_name    = $emp_name;
                   $asset_data->emp_id    = $emp_id;
                   $asset_data->date = $date;
                   $asset_data->company = $company;
                   $asset_data->save();


                   Log::info("Asset Inserted");
                   return response()->json(['status' => "900",'message' => "Asset submitted successfully",
                                             'asset_id' => $asset_id,'category' => $category,'sub_category'=>$sub_category,'brand'=>$brand,'state'=>$state,'city'=>$city,'location'=>$location,'emp_name'=>$emp_name,'emp_id'=>$emp_id
                                              ]);

                   }else {

                      Log::error("Asset ID will be unique");
                      return response()->json(['status' => "907",'message' => "Asset ID will be unique"]);

                    }
                }
                catch (\Exception $e) {

                    Log::error($e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
                }
       			
       		}

    }


    public function bulkAssetInsertAction(Request $request)
    {

       $asset_file =$request->file('asset_file');

       $validator = Validator::make($request->all(), [

              'asset_file' => 'required|max:10000|mimes:csv,xlsx'
         ]);
        
        if($request->hasFile('asset_file')){

            $name = $asset_file->getClientOriginalName();
            $path = '/temp/';
            $asset_file->storeAs($path,$name, 'local'); // saving the file in temp storage


            $collection = \Excel::toCollection(new AssetImport, storage_path('app/temp/'.$name));




            // unlink(storage_path('app/temp/'.$name));  // deleting the file



            return $collection;

        }else{


          return $name;

        }
    }
}
