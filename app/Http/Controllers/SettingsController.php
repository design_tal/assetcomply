<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SubUser;
use App\SubUserSettings;
use Mail;
use App\User;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\VerifyUser;
use App\Mail\SubUserRegisterMail;
use Carbon\Carbon;


class SettingsController extends Controller
{
    public function subUserCreateAndSettings(Request $request)
    {
    	$username = $request->input('username');
    	$company = strtolower($request->input('company'));
    	$name = $request->input('name');
    	$address = $request->input('address');
    	$phone_number = $request->input('phone_number');
    	$email = $request->input('email');
    	$location = $request->input('location');
    	$access_menu = $request->input('access_menu'); // add_asset, report, ticket_raise,
    	$user_type = $request->input('user_type'); //branch_user, field_user

    	 
    	
    	$validator = Validator::make($request->all(), [
    		'username' => 'required',
    		'company' => 'required',
    		'name' => 'required',
    		'address' => 'required',
    		'phone_number' => 'required|digits:10',
    		'email' => 'required|email',
    		'location' => 'required',
    		'access_menu' => 'required',
    		'user_type' => 'required'
    	]);


    	 if($validator->fails()){

    	 	return response()->json(['status' => "901",'message' => $validator->messages()]);

    	 }else{
    	 			$random = Str::random(10);
                    $pass=rand(1,999).$random.rand(1,999);
                    $temp_password=str_shuffle($pass);

                  try{
                    $user_username_checking = User::where('email',$username)->first();
                    $sub_user_username_checking = SubUser::where('username',$username)->first();
                    $sub_usersetting_username_checking = SubUserSettings::where('username',$username)->first();
                    $accmn = implode(',',$access_menu);
                     if(empty($sub_user_username_checking) && empty($sub_usersetting_username_checking && empty($user_username_checking))){

                     		$subuser_personal_data = new SubUser();
                     		$subuser_personal_data->username = $username;
                     		$subuser_personal_data->name=$name;
                     		$subuser_personal_data->address = $address;
                     		$subuser_personal_data->phone_number = $phone_number;
                     		$subuser_personal_data->email = $email;
                     		$subuser_personal_data->location = $location;
                     		$subuser_personal_data->company = $company;
                     		$subuser_personal_data->save();

							$subuser_setting_data = new SubUserSettings();
							$subuser_setting_data->username = $username;
							$subuser_setting_data->company    = $company;
							$subuser_setting_data->user_type    = $user_type;
							
							$subuser_setting_data->access_menu = $accmn ;
							$subuser_setting_data->save();

							$user_login_data = new User();
							$user_login_data->name = $name;
							$user_login_data->email = $username;
							$user_login_data->password = Hash::make($temp_password);
							$user_login_data->save();
                     	 

                     	 \Mail::to($email)->send(new SubUserRegisterMail($username,$name,$temp_password));

		                    Log::info("Email send to this ".$email);
		                    Log::info("Sub User Registration Successfull");
		                    return response()->json(['status' => "900",'message' => "Registration Successfull & Pending for Email Verification"]);

                     }else{

                     		Log::warning("Already Registered ".$email);
		                   return response()->json(['status' => "907",'message' => "Username should be unique"]);

                     }
                   }
                  catch (\Exception $e) {

                  	   Log::error($e->getMessage());
                              return response()->json(['status' => "902",'message' => "Exception while saving Sub User Details"],422);
                  }

    	 }
    }

    public function listOfSubUsers(Request $request)
    {
        $admin_username = $request->input("admin_username");
        $company = $request->input("company");
    }
}
