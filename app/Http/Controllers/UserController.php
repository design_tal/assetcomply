<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;

use App\Userdetails;
use Mail;
use App\Credentials;
use App\User;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\VerifyUser;
use App\Mail\VerifyMail;
use Carbon\Carbon;

class UserController extends Controller
{

	public function registerUser(Request $request)
	{
        $rules = [
                    'firstname' =>'required',
                    'lastname' =>'required',                    
                    'mobile' =>'required|digits:10',
                    'email' =>'required|email',
                    'company' =>'required',
                    'mode' =>'required',
                  ];

        $response = array('response' => '', 'success'=>false);
        $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
               
                return response()->json(['status' => "901",'message' => $validator->messages()]);

            }else{

                $user_email_checking = Userdetails::where('email',$request->input('email'))->first();
                $user_mobile_checking = Userdetails::where('mobile',$request->input('mobile'))->first();
                $user_company_checking = Userdetails::where('company',strtolower($request->input('company')))->first();

                $msg = array();

                if(!empty($user_email_checking) || !empty($user_mobile_checking) || !empty($user_company_checking) ){

                     if(!empty($user_email_checking)){

                         $msg[] = "Email will be unique";

                     }

                      if(!empty($user_mobile_checking)){

                        $msg[] = "Mobile number will be unique";

                     }
                      if(!empty($user_company_checking)){

                        $msg[] = "Company name will be unique";

                     }

                    return response()->json(['status' => "901",'message' => $msg]);

                 }

                   else{

                      // if($request->input('mode') == "strtups"){

                        $this->addCustomerDataToDB($request);

                      // }else{

                      // }
                    

                    $email=$request->input('email');
                    Log::info("Email send to this ".$email);
                    Log::info("Registration Successfull");
                     return response()->json(['status' => "900",'message' => "Registration Successfull & Pending for Email Verification"]);
                  }    

            }

	}

// Adding customer data to DB
  public static function addCustomerDataToDB($request)
  { 
                    $random = Str::random(10);
                    $pass=rand(1,999).$random.rand(1,999);
                    $password=str_shuffle($pass);
                    
                     $userdetails = new Userdetails();
                     $userdetails->username = $request->input('username');
                     $userdetails->firstname=$request->input('firstname');
                     $userdetails->lastname=$request->input('lastname');
                     $userdetails->email=$request->input('email');
                     $userdetails->mobile=$request->input('mobile');
                     $userdetails->company=strtolower($request->input('company'));
                     $userdetails->payment_status=0;
                     $userdetails->source=$request->input('redirect');
                     $userdetails->save();

                     $credentials=new User();
                     $credentials->name=$request->input('firstname').' '.$request->input('lastname');
                     $credentials->email=$request->input('username');
                     $credentials->password=Hash::make($request->input('password'));
                     $credentials->save();


                    

                    $token = sha1(time());

                    $verifyUser = VerifyUser::create([
                      'username' => $request->input('username'),
                      'token' => $token
                    ]);


                    $name=$request->input('firstname').' '.$request->input('lastname');
                    $email=$request->input('email');

                    \Mail::to($email)->send(new VerifyMail($request->input('username'),$token));
                     // $this->sendMail($name,$email,$password);

  }

  public function verifyUser($token)
  {
    $verifyUser = VerifyUser::where('token', $token)->first();
    $verified_check = User::where('email', $verifyUser['username'])->first();
      if(isset($verifyUser)){

             if(!$verified_check['email_verify_status']) {

                $email = $verifyUser['username'];
                $user_email_status = User::where('email', $email)->first();
                $user_email_status->email_verify_status = 1;
                $user_email_status->email_verified_at = Carbon::now();
                $user_email_status->save();

                return response()->json(['status' => "900",'message' => "Email Verified"]);
              } else {
                return response()->json(['status' => "900",'message' => "Email is already verified"]);
              }
        } else {
               return response()->json(['status' => "905",'message' => "Your email can't be identified'"]);
        }
      // return redirect('/login')->with('status', $status);
  }

    static function sendMail($name,$email,$password)
	{
		         
              $to_email=$email;
             
              $data=array('u_id'=>"$to_email",'password'=>"$password",'name'=>"$name");
              Mail::send('mail',$data,function($message) use ($name,$to_email){
                $message->to($to_email)->subject('Login credentials');
              });

	}



	/*login*/
	public function userlogin(Request $request)
	{

		$username=$request->input('username');
		$password=$request->input('password');
    $company=$request->input('company');


		 $validator = Validator::make($request->all(), [
            		    'username' =>'required',
                    'password' =>'required',
                    'company' => 'required'                    
                 
			]);

            
        if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else{

                $login_check = User::where('email', $username)->first();

                if(!empty($login_check))
                {
                  
                  if(!$login_check->email_verify_status){

                    return response()->json(['status' => "906",'message' => "Email is not verified"]); 

                  }else{

                          if(Hash::check($password,$login_check->password))
                            {
                                $check_cmpny = Userdetails::where('company', strtolower($company))->first();
                                if(!empty($check_cmpny)){

                                     $token = $login_check->createToken('Auth Token')->accessToken;

                                     Log::info("Logged In ". $username);

                                     return response()->json(['status' => "900",'message' => "login successfull",'token' => "Bearer ".$token,'username'=>$username,'company'=>$company]);
                                }else{

                                     return response()->json(['status' => "906",'message' => "Company name is not valid"]); 
                                }
                                 

                            }
                             else
                                  {
                                      return response()->json(['status' => "906",'message' => "Wrong username or password"]); 
                                  }

                       }
                  }else{

                       return response()->json(['status' => "906",'message' => "Wrong username or password"]); 
                  }
                }
	}





  /*sub user registration*/
public function registerSubUserbyAdmin(Request $request)
  {
    
    $url = $request->fullUrl();
    
    if (strpos($url, '=') || strpos($url, '&')) {

      Log::warning($request->ip().' is trying to pass values through url');
    
    return response()->json(['status' => "1000",'message' => "parameters should not be passed through url"]);
      die();
     
    }
    

     $validator = Validator::make($request->all(), [
              
              'name' =>'required',                    
              'mobile' =>'required|digits:10|unique:subuserdetails,phone',
              'email' =>'required|email|unique:subuserdetails,email',
              'usertype' =>'required|numeric|min:0|max:2',
              'permissions' =>'required',
              'username' =>'required',
              ]);
     if($validator->fails()) {
    
        //pass validator errors as errors object for ajax response
      $message=$validator->errors()->first();

            return response()->json(['status' => "910",'message' => $message]);
      }
      else{

        /*0 = floor manager, 1= branch manager,2= area manager*/
        $check_usertype=$request->input('usertype');
        if($check_usertype < "0" || $check_usertype > "2"  ){

          die();

        }

            $random = Str::random(10);
            $pass=rand(1,999).$random.rand(1,999);
            $password=str_shuffle($pass);
            
             $userdetails = new SubUser();             
             $userdetails->name=$request->input('name');
             $userdetails->email=$request->input('email');
             $userdetails->phone=$request->input('mobile');
             $userdetails->permissions=$request->input('permissions');
             if($request->input('usertype') == "0")
              {
                $userdetails->user_type="floor manager"; 
              }
              if($request->input('usertype') == "1")
              {
                $userdetails->user_type="branch manager"; 
              }
              if($request->input('usertype') == "2")
              {       
                $userdetails->user_type="area manager";
              }
                   
             $userdetails->added_by=$request->input('username');
             $userdetails->customized_by=$request->input('username');
             $userdetails->save();  
            
            $credentials=new User();
            $name=$request->input('name');
             $credentials->name=$name;
 
  
  $lastId=User::orderBy('id','desc')->pluck('id')->first();
  
  if(empty($lastId)){
  $lastId="001";
  }else{
  $lastId=$lastId+1;
  }
  
  /*print_r(strrev($lastId));die();*/
  $lastId=strrev($lastId);
  $adminumane=$request->input('username');
  $adminumane=explode("/",$adminumane);
  $uname=implode("/",array($adminumane[0],rand(1,999),$lastId));  
  /* print_r(implode("/",array($fcname[0],$fname,$lastId)));die();*/
             $credentials->email=$uname;
             $credentials->password=Hash::make($password);
          
             $credentials->save();           
            $email=$request->input('email');
             $this->sendMail($name,$email,$password,$uname);

             return response()->json(['status' => "900",'message' => "Registration successfull"]);     
     
     
      }

  }


  public function listOfRequestedCustomer()
  {
                try{

                    $customer_data =  \DB::table('userdetails')
                                        ->join('users','userdetails.username', '=', 'users.email')
                                        ->select('userdetails.*','users.email_verify_status','users.mobile_verify_status','users.account_status')
                                        ->orderBy('userdetails.created_at','desc')
                                        ->paginate(10);

                    return response()->json(['status' => "900",'data' => $customer_data]);

                }catch(\Exception $e){

                  return response()->json(['status' => "902",'message' => $e->getMessage()]);
                }   

  }

  public function singleCustomerInfo(Request $request)
  {
        $customer_username = $request->input('customer_username');
        $customer_company = strtolower($request->input('customer_company'));

        try{

                    $customer_data =  \DB::table('userdetails')
                                        ->join('users','userdetails.username', '=', 'users.email')
                                        ->select('userdetails.*','users.email_verify_status','users.mobile_verify_status','users.account_status')
                                        ->where('userdetails.username',$customer_username)
                                        ->where('userdetails.company',$customer_company)
                                        ->first();

                    return response()->json(['status' => "900",'data' => $customer_data]);

                }catch(\Exception $e){

                  return response()->json(['status' => "902",'message' => $e->getMessage()]);
                } 


  }

  public function updateCustomerAccountStatus(Request $request)
  {
     $account_status = $request->input('account_status');
     $username = $request->input('username');
     $company = strtolower($request->input('company'));

         try{

            $customer_account_status_update = Userdetails::where('company', $company)
                                                          ->where('username', $username)
                                                          ->first();

               if(!empty($customer_account_status_update)){

                     $customer_accs_update = User::where('email', $username)
                                                    ->update(['account_status'=>1]);

                     return response()->json(['status' => "900",'message' => "Account status is updated", 'data'=>$customer_account_status_update]);
               }

         }catch (\Exception $e) {

           return response()->json(['status' => "902",'message' => $e->getMessage()]);
         }

  }














}
