<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\AssetInfo;
use App\Userdetails;

class SearchController extends Controller
{
    public function searchAssetByAssetId(Request $request)
    {
    	 $asset_id = $request->input('asset_id');

    	 $validator = Validator::make($request->all(), [

    	 					'asset_id' => 'required'
    	 			]);
    	 if($validator->fails()){

    	 	return response()->json(['status' => "901",'message' => $validator->messages()]);

    	 }else{

    	 		try{

    	 			 $asset_data = AssetInfo::where('asset_id', $asset_id)->first();

    	 			 return response()->json(['status' => "900",'data' => $asset_data]);
    	 		}catch(\Exception $e){

    	 			return response()->json(['status' => "901",'message'=>$e->getMessage()]);
    	 		}

    	 }
    }

    public function listAssetId(Request $request)
    {
    	$company = strtolower($request->input('company'));
    	$location = $request->input('location');
    	$username = $request->input('username');
         
        try{
        	 $username_checking = Userdetails::where('username', $username)->first();

        	     if(empty($username_checking)){
        	     	return response()->json(['status' => "901",'message' => "Something went wrong"]);
        	     }else{

        	     	  $asset_data = AssetInfo::where('location', $location)
        	     	  							->select('asset_id','asset_name')
        	     	  							->orderBy('created_at', 'desc')
        	     	  							->get();
        	     	  return response()->json(['status' => "900",'data' => $asset_data]);
        	     }

        }catch(\Exception $e){

        	return response()->json(['status' => "901",'message' => "Internal error"]);
        }

    }
}
