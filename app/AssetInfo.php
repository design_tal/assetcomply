<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetInfo extends Model
{
   protected $table='asset_info';

   protected $fillable = [
        'asset_id', 'category', 'sub_category', 'brand','model', 'manufacturer','vendor_id'
    ]; 

}