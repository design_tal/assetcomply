-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 12, 2020 at 12:57 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asset_comply`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` bigint(20) NOT NULL,
  `parentlocationname` varchar(200) DEFAULT NULL,
  `location` varchar(500) NOT NULL,
  `locationcode` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `primarylocationhead` varchar(500) DEFAULT NULL,
  `alternativelocationhead` varchar(500) DEFAULT NULL,
  `added_by` varchar(100) NOT NULL,
  `customized_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `parentlocationname`, `location`, `locationcode`, `description`, `primarylocationhead`, `alternativelocationhead`, `added_by`, `customized_by`, `created_at`, `updated_at`) VALUES
(1, 'Habra', 'habra', 'loc120', 'The locationcode field is required.', 'yes', 'no', 'admin', 'admin', '2020-08-01 12:41:51', '2020-08-01 12:41:51'),
(2, 'Sodepur', 'Milangrah', '700113', 'Home', 'ABC', 'XYZ', 'TAL/685/321', 'TAL/685/321', '2020-09-04 09:25:38', '2020-09-04 09:25:38');

-- --------------------------------------------------------

--
-- Table structure for table `asset_info`
--

CREATE TABLE `asset_info` (
  `asset_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_number` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warranty_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condition` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `po_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capitalization_price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_of_life` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `depreciation_rate` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `capitalization_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scrap_value` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `units` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `asset_info`
--

INSERT INTO `asset_info` (`asset_id`, `asset_name`, `category`, `sub_category`, `brand`, `serial_number`, `model`, `description`, `warranty_date`, `condition`, `vendor_id`, `po_number`, `invoice_number`, `invoice_date`, `purchase_price`, `purchase_date`, `capitalization_price`, `end_of_life`, `depreciation_rate`, `capitalization_date`, `scrap_value`, `department`, `state`, `city`, `location`, `units`, `emp_name`, `emp_id`, `date`, `company`, `created_at`, `updated_at`) VALUES
('AS001', 'xuz', 'building', 'flat', 'brand', '1212AS-WE12-1', 'demo', 'kankjsnajksnjkansjkansjknajksnakjnskjansjkansjkanjskanjksnakjsnkajdnjkdfnjkd fs dm dsnds', '2022/06/25', 'second hand', 'ASDWE121', '23/as/oaoak', NULL, NULL, '456', '2002/01/01', NULL, NULL, NULL, NULL, NULL, NULL, 'WB', 'Kolkata', 'Southcity', '2', 'Jhon', 'ASD11000', '2002/01/05', 'vlp', '2020-10-10 12:06:40', '2020-10-10 12:06:40'),
('AS002', 'xuz', 'building', 'flat', 'brand', '1212AS-WE12-1', 'demo', 'kankjsnajksnjkansjkansjknajksnakjnskjansjkansjkanjskanjksnakjsnkajdnjkdfnjkd fs dm dsnds', '2022/06/25', 'second hand', 'ASDWE121', '23/as/oaoak', NULL, NULL, '456', '2002/01/01', NULL, NULL, NULL, NULL, NULL, NULL, 'WB', 'Kolkata', 'Southcity', '2', 'Jhon', 'ASD11000', '2002/01/05', 'vlp', '2020-10-10 12:07:35', '2020-10-10 12:07:35'),
('AS092', 'xuz', 'building', 'flat', 'brand', '1212AS-WE12-1', 'demo', 'kankjsnajksnjkansjkansjknajksnakjnskjansjkansjkanjskanjksnakjsnkajdnjkdfnjkd fs dm dsnds', '2022/06/25', 'second hand', 'ASDWE121', '23/as/oaoak', NULL, NULL, '456', '2002/01/01', NULL, NULL, NULL, NULL, NULL, NULL, 'WB', 'Kolkata', 'Southcity', '2', 'Jhon', 'ASD11000', '2002/01/05', 'vlp', '2020-10-10 12:08:01', '2020-10-10 12:08:01'),
('BSG-0-92', 'xuz', 'building', 'flat', 'brand', '1212AS-WE12-1', 'demo', 'kankjsnajksnjkansjkansjknajksnakjnskjansjkansjkanjskanjksnakjsnkajdnjkdfnjkd fs dm dsnds', '2022/06/25', 'second hand', 'ASDWE121', '23/as/oaoak', NULL, NULL, '456', '2002/01/01', NULL, NULL, NULL, NULL, NULL, NULL, 'WB', 'Kolkata', 'Southcity', '2', 'Jhon', 'ASD11000', '2002/01/05', 'vlp', '2020-10-10 12:13:07', '2020-10-10 12:13:07'),
('BSG/0/92', 'xuz', 'building', 'flat', 'brand', '1212AS-WE12-1', 'demo', 'kankjsnajksnjkansjkansjknajksnakjnskjansjkansjkanjskanjksnakjsnkajdnjkdfnjkd fs dm dsnds', '2022/06/25', 'second hand', 'ASDWE121', '23/as/oaoak', NULL, NULL, '456', '2002/01/01', NULL, NULL, NULL, NULL, NULL, NULL, 'WB', 'Kolkata', 'Southcity', '2', 'Jhon', 'ASD11000', '2002/01/05', 'vlp', '2020-10-10 12:12:57', '2020-10-10 12:12:57');

-- --------------------------------------------------------

--
-- Table structure for table `asset_type`
--

CREATE TABLE `asset_type` (
  `asset_type_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delete_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `asset_type`
--

INSERT INTO `asset_type` (`asset_type_id`, `name`, `delete_status`, `created_at`, `updated_at`) VALUES
('ITA', 'Intangible Assets', 0, NULL, NULL),
('TA', 'Tangible Asset', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asset_type_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delete_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `name`, `parent_id`, `asset_type_id`, `added_by`, `delete_status`, `created_at`, `updated_at`) VALUES
('AH', 'Aircrafts Helicopters', 'VH', 'TA', 'admin_name', 0, '2020-10-06 07:00:18', '2020-10-06 07:00:18'),
('BD', 'Buildings', 'null', 'TA', 'admin_name', 0, '2020-10-06 06:52:47', '2020-10-06 06:52:47'),
('BP', 'Books Periodicals', 'OTA', 'TA', 'admin_name', 0, '2020-10-06 07:02:50', '2020-10-06 07:02:50'),
('BRNDN', 'Brand Names', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:05:39', '2020-10-06 07:05:39'),
('BRPC', 'Bridges Roads Ports Culverts', 'OTA', 'TA', 'admin_name', 0, '2020-10-06 07:02:03', '2020-10-06 07:02:03'),
('CE', 'Computer Equipment', 'null', 'TA', 'admin_name', 0, '2020-10-06 06:59:28', '2020-10-06 06:59:28'),
('CMV', 'Commercial Motor Vehicles', 'VH', 'TA', 'admin_name', 0, '2020-10-06 07:01:01', '2020-10-06 07:01:01'),
('CONSI', 'Concessions and similar', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:07:55', '2020-10-06 07:07:55'),
('CPTSOR', 'Copyrights, patents, trademarks, service and operating rights', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:07:08', '2020-10-06 07:07:08'),
('CR', 'Customer Relationships', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:08:44', '2020-10-06 07:08:44'),
('CSFT', 'Computer Software', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:06:31', '2020-10-06 07:06:31'),
('DEVCO', 'Development costs', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:08:29', '2020-10-06 07:08:29'),
('ERE', 'Emission Reduction Equipment', 'PE', 'TA', 'admin_name', 0, '2020-10-06 06:57:12', '2020-10-06 06:57:12'),
('EXGITA', 'Externally generated intangible assets', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:10:24', '2020-10-06 07:10:24'),
('FB', 'Factory Building', 'BD', 'TA', 'admin_name', 0, '2020-10-06 06:53:51', '2020-10-06 06:53:51'),
('FE', 'Factory Equipments', 'PE', 'TA', 'admin_name', 0, '2020-10-06 06:55:54', '2020-10-06 06:55:54'),
('FF', 'Furniture And Fixtures', 'null', 'TA', 'admin_name', 0, '2020-10-06 06:58:06', '2020-10-06 06:58:06'),
('FFT', 'Furniture and fittings', 'FF', 'TA', 'admin_name', 0, '2020-10-06 06:58:27', '2020-10-06 06:58:27'),
('GDWL', 'Goodwill', 'null', 'ITA', 'admin_name', 0, '2020-10-06 07:04:05', '2020-10-06 07:04:05'),
('IGITA', 'Internally generated intangible assets', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:10:05', '2020-10-06 07:10:05'),
('ITAGDWL', 'Intangible Assets Other than Goodwill', 'null', 'ITA', 'admin_name', 0, '2020-10-06 07:05:11', '2020-10-06 07:05:11'),
('LD', 'Land', 'null', 'TA', 'admin_name', 0, '2020-10-06 06:52:29', '2020-10-06 06:52:29'),
('LHI', 'Leasehold Improvements', 'BD', 'TA', 'admin_name', 0, '2020-10-06 06:54:52', '2020-10-06 06:54:52'),
('LIFR', 'Licences and Franchises', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:06:48', '2020-10-06 07:06:48'),
('LS', 'Livestock', 'OTA', 'TA', 'admin_name', 0, '2020-10-06 07:01:50', '2020-10-06 07:01:50'),
('MPT', 'Mastheads and publishing titles', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:06:16', '2020-10-06 07:06:16'),
('MQ', 'Mines Quarries', 'OTA', 'TA', 'admin_name', 0, '2020-10-06 07:03:19', '2020-10-06 07:03:19'),
('MV', 'Motor Vehicles', 'VH', 'TA', 'admin_name', 0, '2020-10-06 07:00:39', '2020-10-06 07:00:39'),
('OB', 'Other Building', 'BD', 'TA', 'admin_name', 0, '2020-10-06 06:54:14', '2020-10-06 06:54:14'),
('OE', 'Office Equipment', 'null', 'TA', 'admin_name', 0, '2020-10-06 06:59:10', '2020-10-06 06:59:10'),
('OFB', 'Office Building', 'BD', 'TA', 'admin_name', 0, '2020-10-06 06:53:32', '2020-10-06 06:53:32'),
('OPE', 'Other Plant And Equipment', 'PE', 'TA', 'admin_name', 0, '2020-10-06 06:57:46', '2020-10-06 06:57:46'),
('OPRE', 'Other Pollution Reduction Equipment', 'PE', 'TA', 'admin_name', 0, '2020-10-06 06:57:31', '2020-10-06 06:57:31'),
('ORITA', 'Other / residual intangible assets', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:09:23', '2020-10-06 07:09:23'),
('OTA', 'Other Tangible Assets', 'null', 'TA', 'admin_name', 0, '2020-10-06 07:01:32', '2020-10-06 07:01:32'),
('OV', 'Other vehicles', 'VH', 'TA', 'admin_name', 0, '2020-10-06 07:01:13', '2020-10-06 07:01:13'),
('PE', 'Plant And Equipment', 'null', 'TA', 'admin_name', 0, '2020-10-06 06:55:14', '2020-10-06 06:55:14'),
('PLT', 'Plantations', 'OTA', 'TA', 'admin_name', 0, '2020-10-06 07:03:06', '2020-10-06 07:03:06'),
('POAITFA', 'Payments on account, intangible fixed assets', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:09:08', '2020-10-06 07:09:08'),
('PUC', 'Properties under construction', 'BD', 'TA', 'admin_name', 0, '2020-10-06 06:54:36', '2020-10-06 06:54:36'),
('RFMDP', 'Recipes, formulae, models, designs and prototypes', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:08:16', '2020-10-06 07:08:16'),
('RS', 'Railway Sidings', 'OTA', 'TA', 'admin_name', 0, '2020-10-06 07:02:18', '2020-10-06 07:02:18'),
('RSD', 'Residential', 'BD', 'TA', 'admin_name', 0, '2020-10-06 06:53:14', '2020-10-06 06:53:14'),
('SV', 'Ships Vessels', 'VH', 'TA', 'admin_name', 0, '2020-10-06 07:00:05', '2020-10-06 07:00:05'),
('TE', 'Tools and equipment', 'FF', 'TA', 'admin_name', 0, '2020-10-06 06:58:48', '2020-10-06 06:58:48'),
('TIEGITA', 'Total, internally and externally generated intangible assets [default]', 'ITAGDWL', 'ITA', 'admin_name', 0, '2020-10-06 07:09:44', '2020-10-06 07:09:44'),
('VH', 'Vehicles', 'null', 'TA', 'admin_name', 0, '2020-10-06 06:59:45', '2020-10-06 06:59:45');

-- --------------------------------------------------------

--
-- Table structure for table `checkdb`
--

CREATE TABLE `checkdb` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkdb`
--

INSERT INTO `checkdb` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'sayandeep', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` bigint(20) NOT NULL,
  `department` varchar(255) NOT NULL,
  `departmentcode` varchar(200) NOT NULL,
  `contactperson` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `added_by` varchar(200) NOT NULL,
  `customized_by` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `loc_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delete_status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`loc_id`, `location_name`, `company_id`, `added_by`, `delete_status`, `created_at`, `updated_at`) VALUES
('JDV', 'Garia', 'VLP', 'some12', 0, '2020-10-02 11:09:06', '2020-10-02 11:40:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2020_07_24_050117_create_userregistration_table', 1),
(9, '2020_10_01_164611_create_asset_info_table', 2),
(10, '2020_10_02_143848_create_locations_table', 3),
(11, '2020_10_06_114529_create_categories_table', 4),
(12, '2020_10_06_121621_create_asset_type_table', 5),
(13, '2020_10_08_114934_create_sub_users_table', 6),
(15, '2020_10_08_120332_create_user_settings_table', 7),
(16, '2020_10_10_161232_create_asset_info_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('a97c12b3de9a7cbd011f85815872086c1614f96f6debd1c4831d45d5da47c8f323769cca2fa733bd', 101, 1, 'Auth Token', '[]', 0, '2020-07-24 20:36:34', '2020-07-24 20:36:34', '2021-07-24 13:36:34'),
('7fd5e7caaf9314e214ad1a184271d6c31a3d985ae885d095755054945cdffa6ad9b91742e1713eb4', 101, 1, 'Auth Token', '[]', 0, '2020-07-25 19:09:18', '2020-07-25 19:09:18', '2021-07-25 12:09:18'),
('9f1ce4d6446c4901efbbc29ac512ace777605caa4beddbe014ac868457617aa68d23cb760a31f8ad', 101, 1, 'Auth Token', '[]', 0, '2020-07-26 11:08:40', '2020-07-26 11:08:40', '2021-07-26 04:08:40'),
('179ace69d8d1e69b15d5e16f24305a67f3198eb67e333d92fe75e4075326b07aca63ec10c357dc33', 101, 1, 'Auth Token', '[]', 0, '2020-07-26 12:54:29', '2020-07-26 12:54:29', '2021-07-26 05:54:29'),
('4472a8961475c05d5deb334c0d9d73bc8aaee0b41173b72a3caf583d340cfa061755b7e33840e7ee', 101, 1, 'Auth Token', '[]', 0, '2020-07-26 12:57:10', '2020-07-26 12:57:10', '2021-07-26 05:57:10'),
('07b54c6e438bcb1c6e7242e76e740c8959cc29886051b0f087f621619a42dda1da2d3c3c67d0a552', 101, 1, 'Auth Token', '[]', 0, '2020-07-26 12:57:16', '2020-07-26 12:57:16', '2021-07-26 05:57:16'),
('e414d3d48cc36e9e4699597f50ac8437233014c0869cbf624044eb9661e37ba228ec9bca24fb07b6', 101, 1, 'Auth Token', '[]', 0, '2020-07-29 18:50:29', '2020-07-29 18:50:29', '2021-07-29 11:50:29'),
('910a2c95623e6232eed4e70dce30f60e4072d10d88fa4bc63c7c45b6d9b547e0ce8bc51c4ac9ffb2', 101, 1, 'Auth Token', '[]', 0, '2020-08-03 15:23:27', '2020-08-03 15:23:27', '2021-08-03 08:23:27'),
('4e02edf20b52bd2846dd38cca8bd23fc5798d49d6a4e63d6177c4bac598f2c86e034424a0e810baa', 101, 1, 'Auth Token', '[]', 0, '2020-08-03 17:23:20', '2020-08-03 17:23:20', '2021-08-03 10:23:20'),
('59d31ed0c517839ff94275bd2cebd5fe07087adac44f3a9c86fdc7ec6defa2c4b571945715ab986b', 101, 1, 'Auth Token', '[]', 0, '2020-08-03 17:23:36', '2020-08-03 17:23:36', '2021-08-03 10:23:36'),
('c57d1c57182949868a44d2b89328835b467a2d1e8dbc3f2982bdf48abc5e50571ed7cea7af8f0d2a', 101, 1, 'Auth Token', '[]', 0, '2020-08-19 03:29:10', '2020-08-19 03:29:10', '2021-08-18 20:29:10'),
('b14b2be4b7d510218fd296ef6845ff1e1668968f65ede568d2938973b877ac0e6f26d255b887a275', 102, 1, 'Auth Token', '[]', 0, '2020-08-19 23:30:00', '2020-08-19 23:30:00', '2021-08-19 16:30:00'),
('1ffeac684355b4bcca995644d3cb1c1a1756368455502029daff4b02bd40812f9942236d587da956', 102, 1, 'Auth Token', '[]', 0, '2020-08-19 23:48:08', '2020-08-19 23:48:08', '2021-08-19 16:48:08'),
('91988e324da0170d62d144ee0eea0d3e83e690dd103251f0cc9d5a4b800af2bd6d08e8d959ba299b', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 02:59:10', '2020-08-20 02:59:10', '2021-08-19 19:59:10'),
('8f1e404bc20bbdfaa5e133be8ac31b9c29db36b8813cdf08b2888c0edb0d54e95ee2ea92fe20f625', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 03:00:11', '2020-08-20 03:00:11', '2021-08-19 20:00:11'),
('83916b5b588737273591cec2bd4a4af2aa5198c6bd3db27b85c544a000b5053f68139c9c5c266987', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 03:07:36', '2020-08-20 03:07:36', '2021-08-19 20:07:36'),
('2d25272a4b84aea22bf4420456ebb17a1ae511c7edb73bb6603a413ffe6e3b284d228106c7eb03d0', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 03:08:07', '2020-08-20 03:08:07', '2021-08-19 20:08:07'),
('54bbb883e35d93a862f245b0c5668749d102f29060489bcfca0bc3de7b3685fff8669e6a10e9bdd1', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 03:08:48', '2020-08-20 03:08:48', '2021-08-19 20:08:48'),
('b304f7aa66ac2f9f48ca6b51abd3a5f00e2129ca1aa9720e1787b670832d38b0c7bed46364fedcb4', 107, 1, 'Auth Token', '[]', 0, '2020-08-20 03:14:49', '2020-08-20 03:14:49', '2021-08-19 20:14:49'),
('25a01365f9bd43c7798d302cc30e14508c8ae1c128028c8d76b5d84a986b2839d13501f02fbdacb4', 108, 1, 'Auth Token', '[]', 0, '2020-08-20 03:25:42', '2020-08-20 03:25:42', '2021-08-19 20:25:42'),
('112090c86d6253cb504308b7902f87c4a5b67598e5dc15ab38a0d6ab99d4d76f8ada625cace5e336', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 04:08:57', '2020-08-20 04:08:57', '2021-08-19 21:08:57'),
('47f159e956252acefce9fda8619d0ac2d4f4016538c67eb86abec1f42e5965a27a60bdd2322ba9cd', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 04:11:19', '2020-08-20 04:11:19', '2021-08-19 21:11:19'),
('630da1efa44c823dfec1622b139404c1eadb6c52adea54d6901c498a9344177b5d18d622cdc1a98d', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 04:27:28', '2020-08-20 04:27:28', '2021-08-19 21:27:28'),
('35350d9ccd1546f6973a73edc23d8a09f5e768a64b8007f38f695351f5bc1e246e8fa4e49c76773e', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 04:42:36', '2020-08-20 04:42:36', '2021-08-19 21:42:36'),
('b6ec04c8d6db6707be1678f9ed14153c939bcf66691c25129ee73465efcbdcaac2d4f03a5c11c825', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 07:17:56', '2020-08-20 07:17:56', '2021-08-20 00:17:56'),
('f04a09622a749a1b1a0c987eb7efa0ebd52eb6299b229d0f35fa07ec493fda57fcc7605ed4548041', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 07:21:13', '2020-08-20 07:21:13', '2021-08-20 00:21:13'),
('c2e3f502772dc02e7b468a75d1225b88cfdff98c983dd91426289d591945ede7548c3848a9f4f00e', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 19:31:02', '2020-08-20 19:31:02', '2021-08-20 12:31:02'),
('27c790aa9a15be99c2e07799562515c9d635d8ee1735a65aa74452b98dd26a4ae4ae7cd186bb077b', 104, 1, 'Auth Token', '[]', 0, '2020-08-20 19:51:03', '2020-08-20 19:51:03', '2021-08-20 12:51:03'),
('27850c71b2ad902f3cae38dfbab78fd878c85c53d252e1dcbfe7a87c99bc64cdaa0ec6d2ca98f753', 110, 1, 'Auth Token', '[]', 1, '2020-08-21 00:34:41', '2020-08-21 00:34:41', '2021-08-20 17:34:41'),
('c2c11c09c0bb173bced8a323ccf429accbc08acdd02329e139541163fc1e1d57c61ec1eda56cac75', 104, 1, 'Auth Token', '[]', 0, '2020-08-21 21:21:43', '2020-08-21 21:21:43', '2021-08-21 14:21:43'),
('7b2efcce26c18293e83d81f8a4acb2510e3ad145f0b8bb4541952703abb254625b46c0f9b90e25a5', 111, 1, 'Auth Token', '[]', 0, '2020-08-21 21:44:58', '2020-08-21 21:44:58', '2021-08-21 14:44:58'),
('54e814831e5964925275e009d3b3ec64bc256aecfbc03529891167c2dc223a3f3d67d509add8c72e', 111, 1, 'Auth Token', '[]', 0, '2020-08-21 21:45:12', '2020-08-21 21:45:12', '2021-08-21 14:45:12'),
('14912e6c93510cc6856328dfd833b25173877ac5083d699b5727755b61294600902f8622ea628ec7', 108, 1, 'Auth Token', '[]', 0, '2020-08-22 23:31:31', '2020-08-22 23:31:31', '2021-08-22 16:31:31'),
('fc94c189fb3d499fd0770ed92cdfec26cb8798cc5dbf983cdd47e971d656f5d7968decdb0cc37426', 108, 1, 'Auth Token', '[]', 0, '2020-08-22 23:32:18', '2020-08-22 23:32:18', '2021-08-22 16:32:18'),
('b6c4e3056ad1484de2cca2d6c49e1aaad6c64e20cc25c2473f29b0e8c0674cf9c97271a253c8a4fc', 104, 1, 'Auth Token', '[]', 0, '2020-08-22 23:40:31', '2020-08-22 23:40:31', '2021-08-22 16:40:31'),
('bca7525e485e8317d9f8c37803ba5f40750d8bcc5d4676166c925e8fcba5cf38e13bbf9ed20e22f0', 108, 1, 'Auth Token', '[]', 0, '2020-08-23 16:29:19', '2020-08-23 16:29:19', '2021-08-23 09:29:19'),
('ec6f33368fbfc4145e168a849850633beaf5d2a379cd63a6f278d79381fcc31a6ddb8bb2dc949e75', 104, 1, 'Auth Token', '[]', 0, '2020-08-30 17:56:10', '2020-08-30 17:56:10', '2021-08-30 10:56:10'),
('0268f44624b0f47e46c77fb2b191de8d8735f7abe979940f42f47171b429e00f238c6ae41b590c53', 111, 1, 'Auth Token', '[]', 0, '2020-09-02 06:55:58', '2020-09-02 06:55:58', '2021-09-01 23:55:58'),
('36c4e8f6eba71a19693bd79d5e6c84506a9fbae5070a8258cda31a235cfca75dc537e9bb482485dd', 122, 1, 'Auth Token', '[]', 0, '2020-09-03 16:04:14', '2020-09-03 16:04:14', '2021-09-03 09:04:14'),
('9e4bdca4678cf1648e004d3348e56c70640def2331de0999ac3260e8760876562590b7b30febcffe', 121, 1, 'Auth Token', '[]', 0, '2020-09-03 16:51:36', '2020-09-03 16:51:36', '2021-09-03 09:51:36'),
('6337051857333e9781c474b3aea2ba2a69bab42dd3ffe754335230138d055d960b607897c506c0da', 123, 1, 'Auth Token', '[]', 0, '2020-09-04 16:15:31', '2020-09-04 16:15:31', '2021-09-04 09:15:31'),
('72e18cb01f1d3b4264429e32e90503d9acbe501add612474b3de20e096f33947d2c5bf351fdb55d1', 111, 1, 'Auth Token', '[]', 0, '2020-09-05 20:53:27', '2020-09-05 20:53:27', '2021-09-05 13:53:27'),
('bee5082055e7e8d91131cc6fd9372ffe8c2925a8a0ab5165223042f3e8dc6ad47048f60f4cc14729', 111, 1, 'Auth Token', '[]', 0, '2020-09-06 00:22:43', '2020-09-06 00:22:43', '2021-09-05 17:22:43'),
('aac90d26e122ad6e4965c2b30c185da4cc6a3312231b760107bd4a6f8af7853607e7796a1d497543', 111, 1, 'Auth Token', '[]', 0, '2020-09-06 00:22:56', '2020-09-06 00:22:56', '2021-09-05 17:22:56'),
('8c76b1c58c5f60146552b8c2eeac47a34c515ebc62968409bd6eb56a57503397fcec405860fb2753', 123, 1, 'Auth Token', '[]', 0, '2020-09-06 01:46:46', '2020-09-06 01:46:46', '2021-09-05 18:46:46'),
('6aebe427754b1a55b9c45f9fabeee3b502ac0551f4d28d3429c484a24844d03f67948559bafab36f', 123, 1, 'Auth Token', '[]', 0, '2020-09-06 03:03:45', '2020-09-06 03:03:45', '2021-09-05 20:03:45'),
('aebc42a5ce00e4076b55c129735e59e840a37179dec18ef6cee653868d94290cc31b9d1023bba481', 111, 1, 'Auth Token', '[]', 0, '2020-09-10 23:18:09', '2020-09-10 23:18:09', '2021-09-10 16:18:09'),
('3aea2c73712483edc88c73f4379467afe380caa33da6717b35933d50326c4b3b6fa5085fde633402', 108, 1, 'Auth Token', '[]', 0, '2020-09-28 09:05:29', '2020-09-28 09:05:29', '2021-09-28 14:35:29'),
('38b9e53468296d554cf42a6d390c265431448622ae26fe14d73fbc987f8981464a159a73aefe02da', 135, 1, 'Auth Token', '[]', 0, '2020-10-01 10:38:42', '2020-10-01 10:38:42', '2021-10-01 16:08:42'),
('fef3177226bc7bc4478adff5b2c63f085b2a29e6d4d7591974ccf879a30c6591882cce408f2e8cc8', 135, 1, 'Auth Token', '[]', 0, '2020-10-01 11:09:30', '2020-10-01 11:09:30', '2021-10-01 16:39:30'),
('821d4ef33d3a2ee7d402a19b7cfd8512b9b354dbf0b2df8951ad9ae3e1018fd171f19dcf693d0f4d', 135, 1, 'Auth Token', '[]', 0, '2020-10-01 11:12:11', '2020-10-01 11:12:11', '2021-10-01 16:42:11'),
('814c3ba58aace177acc947d3ead684b150976a2432afab7a04c14e2f12adaaa2a93c83abee9b7b05', 135, 1, 'Auth Token', '[]', 0, '2020-10-02 06:50:54', '2020-10-02 06:50:54', '2021-10-02 12:20:54'),
('c6bc609a4124d1b8c5f768680cea64e18f38b7a41ca3fcb280fe67fd2fadf9b561c1fba606f4b8a4', 135, 1, 'Auth Token', '[]', 0, '2020-10-02 06:55:32', '2020-10-02 06:55:32', '2021-10-02 12:25:32');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'ZDqG6WPLhA3itUdhg5H9AlExhzJWKOAODNvuyl8Z', NULL, 'http://localhost', 1, 0, 0, '2020-07-24 19:05:47', '2020-07-24 19:05:47'),
(2, NULL, 'Laravel Password Grant Client', 'kk2G5X97oW45Mp1j7ptO2EQaiERDBKkYN5NI7NAM', 'users', 'http://localhost', 0, 1, 0, '2020-07-24 19:05:50', '2020-07-24 19:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-07-24 19:05:50', '2020-07-24 19:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_users`
--

CREATE TABLE `sub_users` (
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_users`
--

INSERT INTO `sub_users` (`username`, `name`, `address`, `phone_number`, `email`, `location`, `company`, `created_at`, `updated_at`) VALUES
('abc-polo', 'Demo user', 'sulekha, jadavpur', '7589652580', 'demouser@gmail.com', 'southcity', 'vlp', '2020-10-08 11:07:25', '2020-10-08 11:07:25');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` bigint(20) NOT NULL,
  `unit` varchar(200) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `added_by` varchar(200) NOT NULL,
  `customized_by` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `unit`, `description`, `added_by`, `customized_by`, `created_at`, `updated_at`) VALUES
(1, '25', 'kjdbshujdj', 'anirudh.thinkagainlab@gmail.com', 'anirudh.thinkagainlab@gmail.com', '2020-08-30 13:21:13', '2020-08-30 13:21:13'),
(2, '2', 'Electronic', 'TAL/685/321', 'TAL/685/321', '2020-09-06 16:16:11', '2020-09-06 16:16:11');

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `subdomain` varchar(255) DEFAULT NULL,
  `company` varchar(100) NOT NULL,
  `payment_status` tinyint(1) NOT NULL DEFAULT 0,
  `source` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile`, `subdomain`, `company`, `payment_status`, `source`, `created_at`, `updated_at`) VALUES
(32, '', 'Kevin', 'jones', 'test@test.com', 1234567890, 'apdjsi12', 'kdjbuh12', 0, 'self', '2020-08-19 19:20:14', '2020-08-19 19:20:14'),
(29, '', 'John', 'Banerjee', 'banerjeepushpak@gmail.comkk', 1020301055, 'googlekgjjkffif1223.bom', 'hdjdfjvvvvfvnknjk', 0, 'self', '2020-08-19 16:13:36', '2020-08-19 16:13:36'),
(30, '', 'Kevin', 'jones', 'kj@gmail.com', 9432200500, 'app', 'app', 0, 'self', '2020-08-19 19:14:47', '2020-08-19 19:14:47'),
(31, '', 'Kevin', 'jones', 'anirudh.thinkagainlab@gmail.com', 9432050345, 'apper', 'apper12', 0, 'self', '2020-08-19 19:17:38', '2020-08-19 19:17:38'),
(33, '', 'hjhgf', 'kjbjg', 'lknh@kjhig.com', 9432050123, 'kjuggi', 'hfufuy', 0, 'self', '2020-08-19 19:28:37', '2020-08-19 19:28:37'),
(34, '', 'Kevin', 'Markum', 'shadymnx@gmail.com', 1236547890, 'jvdashdvu', 'kjdhsds', 0, 'self', '2020-08-19 20:12:56', '2020-08-19 20:12:56'),
(35, '', 'Sayandeep', 'Majumdar', 'smajumdar1993@gmail.com', 8017730776, 'ray', 'ray', 0, 'self', '2020-08-19 20:24:10', '2020-08-19 20:24:10'),
(36, '', 'Pushpak', 'Banerjee', 'banerjepushpak@gmail.comlll', 7890056689, 'sexy', 'very good', 0, 'self', '2020-08-20 16:53:05', '2020-08-20 16:53:05'),
(37, '', 'Pushpak', 'Banerjee', 'banerjee.pushpak.tal@gmail.comnb', 1020304578, 'hello', 'very gggg', 0, 'self', '2020-08-20 17:08:40', '2020-08-20 17:08:40'),
(38, '', 'Sachin', 'Trivedi', 'sachin.trivedi@gmail.com', 9903037609, 'volition', 'Volition Business Solutions LLP', 0, 'self', '2020-08-21 11:24:44', '2020-08-21 11:24:44'),
(39, '', 'xyz', 'klp', 'msayandeep1993@gmail.com', 8017730775, 'techifylab', 'as', 0, 'self', '2020-08-21 14:33:19', '2020-08-21 14:33:19'),
(62, '', 'Pushpak', 'Banerjee', 'banerjeepushpak@gmail.com', 1020304570, 'pushpak', 'Manlysoft pvt ltd', 0, 'self', '2020-08-22 17:36:05', '2020-08-22 17:36:05'),
(65, '', 'Sujit', 'Pal', 'spal35151@gmail.com', 9163770810, 'xyz', 'TAL', 0, 'self', '2020-09-04 09:07:17', '2020-09-04 09:07:17'),
(82, 'some12', 'asset', 'tracsk', '1993msayandeep@gmail.com', 123456985, NULL, 'vlp', 0, 'self', '2020-10-01 13:40:48', '2020-10-01 13:40:48'),
(84, 'kkk', 'asset', 'tracsk', '545@gmail.com', 9797777794, NULL, 'polo', 0, 'self', '2020-10-02 16:03:24', '2020-10-02 16:03:24'),
(85, 'kknnk', 'asset', 'tracsk', 'ghh6@gmail.com', 7896325120, NULL, 'pomnlo', 0, 'self', '2020-10-02 16:04:37', '2020-10-02 16:04:37'),
(86, 'asds22', 'asset', 'tracsk', 'sdsd2@gmail.com', 6464654646, NULL, 'wew2', 0, 'self', '2020-10-02 16:10:18', '2020-10-02 16:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_verify_status` tinyint(1) NOT NULL DEFAULT 0,
  `mobile_verify_status` tinyint(1) NOT NULL DEFAULT 0,
  `account_status` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `email_verify_status`, `mobile_verify_status`, `account_status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(101, 'sayandeep', 'sayan@gmail.com', NULL, 0, 0, 0, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, NULL),
(102, 'John Banerjee', 'banerjeepushpak@gmail.com', NULL, 0, 0, 0, '$2y$10$Ef6qj.Js05.RaVVA1OfSjewmUEBEpj7kFWPlUCbWtGuyUz7Gp6e7C', NULL, '2020-08-19 23:13:36', '2020-08-19 23:13:36'),
(103, 'Kevin jones', 'kj@gmail.com', NULL, 0, 0, 0, '$2y$10$gLKmP5g9Uoyrf.DO7ZGFkOFjGANVYDpfXc748Z1Wt8NPaM.fR0gwq', NULL, '2020-08-20 02:14:47', '2020-08-20 02:14:47'),
(104, 'Kevin jones', 'anirudh.thinkagainlab@gmail.com', NULL, 0, 0, 0, '$2y$10$1bvWVyzTrPXQUPgF8Xgfv.MfxG1TWRKlpr8FmTzjYEDyG59lBUaci', NULL, '2020-08-20 02:17:38', '2020-08-20 02:17:38'),
(105, 'Kevin jones', 'test@test.com', NULL, 0, 0, 0, '$2y$10$u22pUhHSabuiwYOC9XnoO.7MMPsVzLhNqDMlU5g6V/J9lOcFM/D.K', NULL, '2020-08-20 02:20:14', '2020-08-20 02:20:14'),
(106, 'hjhgf kjbjg', 'lknh@kjhig.com', NULL, 0, 0, 0, '$2y$10$Wz3y.uLK55rXRqJ2zQjfu.0tYmyXm0AG3RmRDPVe7a6UbEksvDwPi', NULL, '2020-08-20 02:28:37', '2020-08-20 02:28:37'),
(107, 'Kevin Markum', 'shadymnx@gmail.com', NULL, 0, 0, 0, '$2y$10$bhCWmkBqPEh0ey2VnvvYfOq5fwmPS2nwe1tf1FPBNFHVsG6fdwfvq', NULL, '2020-08-20 03:12:56', '2020-08-20 03:12:56'),
(108, 'Sayandeep Majumdar', 'smajumdar1993@gmail.com', NULL, 0, 0, 0, '$2y$10$cYknHwAgY0.V6n7VV26MJOXg1ScsFOmDGcl2z0rzRVrTg7pAwM0bu', NULL, '2020-08-20 03:24:10', '2020-08-20 03:24:10'),
(109, 'Pushpak Banerjee', 'banerjepushpak@gmail.com', NULL, 0, 0, 0, '$2y$10$1mI2G6tEFG2OZJEvYzvaGOvij1VL0/LLuHyQ20.IreRV16jPsk7J.', NULL, '2020-08-20 23:53:05', '2020-08-20 23:53:05'),
(110, 'Pushpak Banerjee', 'banerjee.pushpak.tal@gmail.com', NULL, 0, 0, 0, '$2y$10$Ea/Gcg2Jn5rFhbcFlB03uODcNGgEIRbsGCpFV.YlaomDCa6Wu3TVm', NULL, '2020-08-21 00:08:40', '2020-08-21 00:08:40'),
(111, 'Sachin Trivedi', 'sachin.trivedi@gmail.com', NULL, 0, 0, 0, '$2y$10$.3O/uOgmr1W7UlywIQf0sul6RH.7bXNaj4t1FosT73bNkpWfCkl0.', NULL, '2020-08-21 18:24:44', '2020-08-21 18:24:44'),
(113, 'xyz klp', 'msayandeep1993@gmail.com', NULL, 0, 0, 0, '$2y$10$1qSCWRWJXzA2Jw8xtLhGWunnc3WEJxv5VoJBtzOy7AjKrC61K6O36', NULL, '2020-08-21 21:33:19', '2020-08-21 21:33:19'),
(114, 'Pushpak Banerjee', 'TAL/Pushpak/311', NULL, 0, 0, 0, '$2y$10$j2D9ZR12rdb4H5/pIUS.KOSiqRFG4./.auO/ZPC0.03lB3fvGaq2y', NULL, '2020-08-22 22:14:42', '2020-08-22 22:14:42'),
(115, 'Pushpak Banerjee', 'TAL/Pushpak/411', NULL, 0, 0, 0, '$2y$10$5ci2O0Gf5.NtvxflZ3FwXeQJf9EQy5vLX1..nHmV9pmxLMEPTQ0Ru', NULL, '2020-08-22 22:18:29', '2020-08-22 22:18:29'),
(116, 'Pushpak Banerjee', 'Manlysoft/Pushpak/611', NULL, 0, 0, 0, '$2y$10$6yYvs.C2qSO27SFRwrG5PO0ZURLUVUjfKrjhid57oD0r.DZXu2qga', NULL, '2020-08-23 00:31:42', '2020-08-23 00:31:42'),
(117, 'Pushpak Banerjee', 'Manlysoft/786/711', NULL, 0, 0, 0, '$2y$10$FwtcLgUn4Fu5WkZjIPC6e.WZAf7n54YTUH9xael4a33ijY6NQQmv6', NULL, '2020-08-23 00:36:05', '2020-08-23 00:36:05'),
(118, 'Helo', 'TAl/31/811', NULL, 0, 0, 0, '$2y$10$KbAZypmZBzg9uRWEKEs9AeXUWMTNPvLPGiEUYCVeWMZHh.IHkLXEa', NULL, '2020-08-24 21:08:07', '2020-08-24 21:08:07'),
(119, 'Helo', 'TAl/626/911', NULL, 0, 0, 0, '$2y$10$QWXyCqa4hLTAgx0FN7PFjObocyacWBuO6OVvVLhFED4HQsl3DIiie', NULL, '2020-08-24 21:16:45', '2020-08-24 21:16:45'),
(120, 'Helo', 'TAl/902/021', NULL, 0, 0, 0, '$2y$10$MtahGEfzi.brBo0otFleTunvZhyenwIWcgNZQSlriBH1Atq1aEgD6', NULL, '2020-08-24 21:17:59', '2020-08-24 21:17:59'),
(121, 'Sujit Pal', 'Think/813/121', NULL, 0, 0, 0, '$2y$10$k6hwzHFeIW/REsQhAXIwIezQRuTlhS56tHk5mEWxhPsUqsM6C8TCC', NULL, '2020-09-03 03:54:05', '2020-09-03 03:54:05'),
(122, 'Nitin Trivedi', 'ABC/914/221', NULL, 0, 0, 0, '$2y$10$ybiJGbTamdJc89i1RDPcUeliTiwudAXLG461X9WddIbtABDuQvms.', NULL, '2020-09-03 16:00:05', '2020-09-03 16:00:05'),
(123, 'Sujit Pal', 'TAL/685/321', NULL, 0, 0, 0, '$2y$10$VuuLiB15.csR5gfVSQ7.b.Hl3snLZFE9gviwGOg4s99v0NrRcSJSK', NULL, '2020-09-04 16:07:17', '2020-09-04 16:07:17'),
(135, 'asset tracsk', 'some12', NULL, 1, 0, 1, '$2y$10$UdpUtzUVfZ3K03jdKttx4.SdazCGwetiPtln/tY5edSL30NWlZ.Ye', NULL, '2020-10-01 08:10:51', '2020-10-07 09:28:57'),
(136, 'asset tracsk', 'kkk', NULL, 0, 0, 0, '$2y$10$STewhLgONX89z0f6glBlBeBZoUpji9L2J0QtIiUisQGg2rOJ8T/3S', NULL, '2020-10-02 10:33:25', '2020-10-02 10:33:25'),
(137, 'asset tracsk', 'kknnk', NULL, 0, 0, 0, '$2y$10$0NZVLX7nmPMG.rgeDg4O7eaxD3gTnq0kzIRFIx3GVwhNvCi68zU9S', NULL, '2020-10-02 10:34:37', '2020-10-02 10:34:37'),
(138, 'asset tracsk', 'asds22', NULL, 0, 0, 0, '$2y$10$6THY0PLZ/8kpQwIc/fuokekZVtfexIQNzGzPtAZ.oFLMfsouxzUI6', NULL, '2020-10-02 10:40:18', '2020-10-02 10:40:18'),
(141, 'Demo user', 'abc-polo', NULL, 0, 0, 0, '$2y$10$3py.nArb9pysVNT840ME..YKaTxlcF3HbfZ.xzgAQTrieTP6B4LFq', NULL, '2020-10-08 11:07:26', '2020-10-08 11:07:26');

-- --------------------------------------------------------

--
-- Table structure for table `user_settings`
--

CREATE TABLE `user_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_settings`
--

INSERT INTO `user_settings` (`id`, `username`, `company`, `user_type`, `access_menu`, `created_at`, `updated_at`) VALUES
(4, 'abc-polo', 'vlp', 'floor_manager', 'add_asset,ticket_raise', '2020-10-08 11:07:25', '2020-10-08 11:07:25');

-- --------------------------------------------------------

--
-- Table structure for table `vendorcustomer`
--

CREATE TABLE `vendorcustomer` (
  `id` bigint(20) NOT NULL,
  `vendorname` varchar(200) NOT NULL,
  `contactperson` varchar(200) DEFAULT NULL,
  `contactnumber` bigint(10) DEFAULT NULL,
  `address1` varchar(500) DEFAULT NULL,
  `address2` varchar(500) DEFAULT NULL,
  `registrationcode` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `postalcode` varchar(10) DEFAULT NULL,
  `pan` varchar(20) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cst` varchar(20) DEFAULT NULL,
  `customer` varchar(500) DEFAULT NULL,
  `added_by` varchar(100) DEFAULT NULL,
  `customized_by` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `token` varchar(500) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`id`, `username`, `token`, `created_at`, `updated_at`) VALUES
(4, 'some12', '56a4b204672bf67344945142443d5dff4fb81037', '2020-10-01 13:40:51', '2020-10-01 13:40:51'),
(5, 'kkk', 'dd0806cfa8986836da307cfbd5be9a7ac206c83a', '2020-10-02 16:03:25', '2020-10-02 16:03:25'),
(6, 'kknnk', '6f756ac58fb6be5b13c87edac69335d2cb581f35', '2020-10-02 16:04:37', '2020-10-02 16:04:37'),
(7, 'asds22', '636010becc3d69ef1e870f59def89f0a8c6c9c17', '2020-10-02 16:10:18', '2020-10-02 16:10:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_info`
--
ALTER TABLE `asset_info`
  ADD PRIMARY KEY (`asset_id`);

--
-- Indexes for table `asset_type`
--
ALTER TABLE `asset_type`
  ADD PRIMARY KEY (`asset_type_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `checkdb`
--
ALTER TABLE `checkdb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`loc_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `sub_users`
--
ALTER TABLE `sub_users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendorcustomer`
--
ALTER TABLE `vendorcustomer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `checkdb`
--
ALTER TABLE `checkdb`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `user_settings`
--
ALTER TABLE `user_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vendorcustomer`
--
ALTER TABLE `vendorcustomer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
