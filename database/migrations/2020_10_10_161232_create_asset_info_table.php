<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_info', function (Blueprint $table) {
            $table->string('asset_id',150)->primary();
            $table->string('asset_name', 255);
            $table->string('category', 100);
            $table->string('sub_category', 255);
            $table->string('brand', 100);
            $table->string('serial_number', 190)->nullable();
            $table->string('model', 150);
            $table->string('description',300);
            $table->string('warranty_date',100)->nullable();
            $table->string('condition',100)->nullable();
            $table->string('vendor_id', 100);
            $table->string('po_number', 100)->nullable();
            $table->string('invoice_number',100)->nullable();
            $table->string('invoice_date',100)->nullable();
            $table->string('purchase_price',100)->nullable();
            $table->string('purchase_date',100)->nullable();
            $table->string('capitalization_price',100)->nullable();
            $table->string('end_of_life',100)->nullable();
            $table->string('depreciation_rate',100)->nullable();
            $table->string('capitalization_date',100)->nullable();
            $table->string('scrap_value',100)->nullable();
            $table->string('department',100)->nullable();
            $table->string('state',100);
            $table->string('city',100);
            $table->string('location',100);
            $table->string('units',100);
            $table->string('emp_name',100);
            $table->string('emp_id',100);
            $table->string('date',100);
            $table->string('company',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_info');
    }
}
