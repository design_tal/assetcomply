<?php 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});

Route::group(['middleware' => 'cors'], function () {

Route::prefix('v1')->group(function(){

Route::get('/getId','QRCOntroller@generateId');
Route::post('/submitqr','QRCOntroller@saveQR');


Route::post('/user/registration/','UserController@registerUser');

Route::get('/user/verify/{token}', 'UserController@verifyUser');


Route::post('/user/login','UserController@userlogin');

Route::post('/asset/upload','AssetController@bulkAssetInsertAction');


// Route::group(['middleware' => ['auth:api']], function () {


// Asset Adding 
Route::post('/asset/add','AssetController@addSingleAsset');
Route::post('/search/asset/','SearchController@searchAssetByAssetId');
Route::post('/asset/id/list','SearchController@listAssetId');

// Asset End


// Location adding
Route::post('/location/add','ApplicationSettingsController@setLocationForCompany');
Route::get('/location/show/{company_id}','ApplicationSettingsController@listLocationsByCompany');
Route::get('/location/delete/{location_id}/{company_id}','ApplicationSettingsController@deleteSingleLocation');
Route::post('/location/update','ApplicationSettingsController@updateSingleLocation');
// Location End


// User settings (QR code field choosing)



	 // }); // Middleware End
Route::post('/user/settings','SettingsController@subUserCreateAndSettings');

});   //Prefix End


// Admin Routes
Route::prefix('v1/admin')->group(function(){


Route::post('/user/registration/','UserController@registerUser');

Route::get('/user/verify/{token}', 'UserController@verifyUser');


Route::post('/user/login','UserController@userlogin');

// Route::group(['middleware' => ['auth:api']], function () {


// Category 
Route::post('/category/add','CategoriesController@addCategory');
Route::get('/category/list/sub','CategoriesController@listOfCategoriesWithSubcategory');
Route::get('/category/list/parent','CategoriesController@listOfCategoriesWithParent');
Route::post('/category/list/single/parent','CategoriesController@listOfCategoriesWithSingleParent');

// Category End

// Customer 

Route::get('/customer/list/','UserController@listOfRequestedCustomer');
Route::post('/customer/single/info','UserController@singleCustomerInfo');
Route::post('/customer/update/account-status','UserController@updateCustomerAccountStatus');

// Customer End

	 // }); // Middleware End

});   //Prefix End



}); //CORS Validation end









// /*application settings*/

// /*asset property*/
// Route::post('/v1/asset/setproperty','ApplicationSettingsController@assetPropertySet');

// /*address*/
// Route::post('/v1/asset/setaddress','ApplicationSettingsController@assetAddressSet');

// /*status*/
// Route::post('/v1/asset/setStatus','ApplicationSettingsController@assetStatusSet');

// /*department*/
// Route::post('/v1/asset/createDepartment','ApplicationSettingsController@createDepartment');


// /*meter type*/
// Route::post('/v1/asset/createmetertype','ApplicationSettingsController@createMeterType');

// /*condition set*/
// Route::post('/v1/asset/setCondition','ApplicationSettingsController@createCondition');

// /*reason */
// Route::post('/v1/asset/setReason','ApplicationSettingsController@createReason');

// /*vendor*/

// Route::post('/v1/asset/createVendor','ApplicationSettingsController@createVendorCustomer');

// /*tax*/

// Route::post('/v1/asset/createTax','ApplicationSettingsController@createTax');

// /*activity type*/
// Route::post('/v1/asset/createActivityType','ApplicationSettingsController@createActivityType');

// /*movement status*/

// Route::post('/v1/asset/createMovementStatus','ApplicationSettingsController@createMovementStatus');

// /*unit */
// Route::post('/v1/asset/createUnit','ApplicationSettingsController@createUnit');

// /*brand*/
// Route::post('/v1/asset/createBrand','ApplicationSettingsController@createBrand');

// /*model*/
// Route::post('/v1/asset/createModel','ApplicationSettingsController@createModel');


// /*purchase order*/

// Route::post('/v1/asset/createPurchaseOrder','ApplicationSettingsController@createPurchaseOrder');